import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: true,
            list: []
        }
        this.showHandler = this.showHandler.bind(this);
        this.hideHandler = this.hideHandler.bind(this);
        this.refreshHandler = this.refreshHandler.bind(this);
    }

    showHandler() {
        this.setState({show: true});
    }

    hideHandler() {
        this.setState({show: false});
    }


    refreshHandler() {
        this.setState({show: false, list: []});
    }


    render() {
        return (
            <div className='App'>
                {
                    this.state.show ?
                        <button onClick={this.hideHandler}>Hide</button> :
                        <button onClick={this.showHandler}>Show</button>
                }
                <button onClick={this.refreshHandler}>Refresh</button>
                {
                    this.state.show ?
                        <TodoList list={this.state.list} onChange={(list) => {this.setState({list: list})}}/> :
                        null
                }
            </div>
        );
    }
}


export default App;
