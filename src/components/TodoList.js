import React, {Component} from 'react';
import './todolist.less';
import Items from './Items';

class TodoList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      itmesNum: 0
    };
    this.clickHandler = this.clickHandler.bind(this);
    console.log('constructor');
  }

  componentDidMount() {
    console.log('componentDidMount');
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('componentDidUpdate');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
  }

  clickHandler() {
    this.props.onChange(this.props.list.concat([{ content: 'List title' + this.props.list.length}]))
  }

  render() {
    console.log('render');
    return (
        <div>
          <button onClick={this.clickHandler}>ADD</button>
          <ul>
            {this.props.list.map((item) => (<li key={item.content}>{item.content}</li>) )}
          </ul>
        </div>
    );
  }
}

export default TodoList;

